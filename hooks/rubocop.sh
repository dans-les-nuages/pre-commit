#!/bin/bash

if ! rubocop --require rubocop-rails -a >/dev/null 2>&1
then
  exit 1
fi
exit 0
