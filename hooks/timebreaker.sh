#!/usr/bin/zsh

h=$(date +%H)
echo "$h"
case $h in
  <0-8>)
    westop=0 && echo "OK";;
  <12-13>)
    westop=0 && echo "OK";;
  <18-23>)
    westop=0 && echo "OK";;
  *)
    westop=1 && echo "Not the good slot time";;
esac

exit $westop
