#!/bin/bash

bundle exec i18n-tasks health >/dev/null 2>&1
[ $? -gt 1 ] && exit 1
exit 0
