#!/bin/bash

if ! gitleaks detect -v >/dev/null 2>&1
then
  echo "Gitleaks detected something weird"
  exit 1
fi
exit 0
