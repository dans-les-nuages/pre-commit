#!/bin/bash

if ! bundler-audit check --update >/dev/null 2>&1
then
  exit 1
fi
exit 0
