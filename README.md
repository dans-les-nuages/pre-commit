# Pre-Commit-Hooks

Include security by design, in developments.

Some out-of-the-box **secops** hooks for pre-commit.

## Hooks

- General
- Ruby on Rails
- Python

## Usage

Install pre-commit

    python3 -m pip install pre-commit

Create the `.pre-commit-config.yaml` file in your repo,
 choosing the hooks you want :

    # .pre-commit-config.yaml
    repos:
    -   repo: https://gitlab.com/dans-les-nuages/pre-commit
        rev: 1.1.1
        hooks:
        -   id: brakeman
        -   id: gitleaks
        -   ...

Install the hooks and run a first test :

    pre-commit install
    pre-commit run --all-files
